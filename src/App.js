import React from 'react';
import logo from './assets/icons/m&m-logo.svg'
import Router from './pages/Router'
import WithHelloMessage from './components/HelloComponent';
import './App.css';

const App = () => (
  <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <div>
        React test
      </div>
    </header>
    <Router />
  </div>
);

export default WithHelloMessage(App);