import React from 'react';
import PropTypes from 'prop-types';
import Auth from "../../pages/LoginPage/Auth";
import WithHelloMessage from '../HelloComponent';
import { StyledButton } from './Wrapper';

const LogoutButton = ({history}) =>
<StyledButton
  onClick={() => {
    Auth.logout(() => {
      history.push("/");
    });
  }}
  >
  Logout
</StyledButton>

LogoutButton.propTypes = {
  history: PropTypes.object,
};

export default WithHelloMessage(LogoutButton);
