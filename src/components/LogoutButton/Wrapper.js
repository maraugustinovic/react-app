import styled from 'styled-components';

export const StyledButton = styled.button`
  position: absolute;
  top: 20px;
  right: 20px;
  padding: 5px 10px;
`;