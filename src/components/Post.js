import React from 'react';
import PropTypes from 'prop-types';
import WithHelloMessage from './HelloComponent';
import UsersAndCommentsFilter from './UsersAndCommentsFilter';

const Post = ({usersPost, comments}) => 
  <div className="container">
    <div className="jumbotron-div col s12">
      <div className="alert alert-primary">
        <p>Post: {usersPost.body}</p>
        <UsersAndCommentsFilter usersPost={usersPost} comments={comments} />
      </div>
    </div>
  </div>

Post.propTypes = {
  usersPost: PropTypes.any,
  comments: PropTypes.array,
};

export default WithHelloMessage(Post);
