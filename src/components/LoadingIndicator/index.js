import React from 'react';
import PropTypes from 'prop-types';
import WithHelloMessage from '../../components/HelloComponent';
import {
  Loader,
  Wrapper,
} from './Wrapper';
import loaderIcon from './assets/Eclipse-1.1s-64px.svg';

function LoadingIndicator({ size }) {
  return (
    <Wrapper>
      <Loader size={size}>
        <img src={loaderIcon} alt="" />
      </Loader>
    </Wrapper>
  );
}

LoadingIndicator.propTypes = {
  size: PropTypes.number,
};

LoadingIndicator.defaultProps = {
  size: 32,
};

export default WithHelloMessage(LoadingIndicator);
