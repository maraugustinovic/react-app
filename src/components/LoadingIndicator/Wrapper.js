import styled from 'styled-components';

export const Loader = styled.div`
  width: ${(props) => props.size}px;
  height: ${(props) => props.size}px;
`;

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 15%;
  margin-bottom: 15%;
`;
