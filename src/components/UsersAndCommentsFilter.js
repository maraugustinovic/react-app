import React from 'react';
import PropTypes from 'prop-types';
import WithHelloMessage from './HelloComponent';

const UsersAndCommentsFilter = ({usersPost, comments}) =>
  <>
    <p className="alert alert-danger">by user: {usersPost.name}</p>
    {comments.filter((comment) => comment.postId === usersPost.id).map(comment => (
      <p className="alert alert-success" key={comment.id}>Comment: {comment.body}</p>
    ))}
  </>

UsersAndCommentsFilter.propTypes = {
  usersPost: PropTypes.any,
  comments: PropTypes.array,
};

export default WithHelloMessage(UsersAndCommentsFilter);
