import React from 'react';
import PropTypes from 'prop-types';
import WithHelloMessage from './HelloComponent';
import UsersAndCommentsFilter from './UsersAndCommentsFilter';
import { NavLink } from 'react-router-dom'

const PostsList = ({usersPosts, comments}) => 
  <div className="container">
    <div className="jumbotron-div col s12">
      {usersPosts.map(usersPost => (
        <div key={usersPost.id} className="alert alert-primary">
          <NavLink to={`/post/${usersPost.id}`}>Post: {usersPost.body}</NavLink>
          <UsersAndCommentsFilter usersPost={usersPost} comments={comments} />
        </div>
        ))
      }
    </div>
  </div>

PostsList.propTypes = {
  usersPost: PropTypes.array,
  comments: PropTypes.array,
};

export default WithHelloMessage(PostsList);
