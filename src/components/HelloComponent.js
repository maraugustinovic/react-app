import React from 'react';

const message = "Hello from";

function WithHelloMessage(WrappedComponent) {
  return class extends React.Component {
    render() {
      console.log(`${message} ${getDisplayName(WrappedComponent)} component`);
      return <WrappedComponent {...this.props} />
    }
  }
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default WithHelloMessage;
