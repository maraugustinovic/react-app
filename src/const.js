export const EMAIL = 'martin@test.com';
export const PASSWORD = 'martianTest1';
export const IS_LOGGED_IN = 'isLoggedIn';
export const API_BASE_URL = 'https://jsonplaceholder.typicode.com';
//https://stackoverflow.com/questions/52047548/response-for-preflight-does-not-have-http-ok-status-in-angular
// export const API_BASE_URL = 'https://demo.martian.agency/api';
export const POSTS_URL = `${API_BASE_URL}/posts`;
export const getSpecificPost = postId => `${API_BASE_URL}/posts/${postId}`;
export const getSpecificPostComments = postId => `${API_BASE_URL}/posts/${postId}/comments`;
export const COMMENTS_URL = `${API_BASE_URL}/comments`;
export const USERS_URL = `${API_BASE_URL}/users`;
export const ACCESS_TOKEN = 'bWFydGlhbmFuZG1hY2hpbmU=';
export const HEADERS = {
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*',
  'X-Auth': ACCESS_TOKEN
};
