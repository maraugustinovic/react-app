import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute'
import LoginPage from './LoginPage';
import HomePage from './HomePage';
import PostPage from './PostPage';
import WithHelloMessage from '../components/HelloComponent';


const Router = () => (
  <Switch>
    <Route exact path="/" component={LoginPage} />
    <ProtectedRoute exact path="/app" component={HomePage} />
    <ProtectedRoute exact path="/post/:id" component={PostPage} />
    <Route path="*" component={() => "404 NOT FOUND"} />
  </Switch>
)

export default WithHelloMessage(Router);
