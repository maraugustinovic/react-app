import React from 'react';
import axios from 'axios';
import { Helmet } from 'react-helmet';
import { Wrapper, SearchInput, StyledParagraph } from './Wrapper';
import WithHelloMessage from '../../components/HelloComponent';
import LoadingIndicator from '../../components/LoadingIndicator';
import PostsList from '../../components/PostsList';
import LogoutButton from '../../components/LogoutButton'
import {
  POSTS_URL,
  COMMENTS_URL,
  USERS_URL,
  HEADERS,
 } from '../../const';

class HomePage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      users: [],
      comments: [],
      usersPosts: [],
      isLoading: true,
      filtered: [],
    };

    this.getPostsData = this.getPostsData.bind(this);
    this.filterList = this.filterList.bind(this);
  }

  componentDidMount() {
    this.getPostsData();
  }

  getPostsData() {
    Promise.all([
      axios.get(POSTS_URL, {headers: HEADERS}),
      axios.get(USERS_URL, {headers: HEADERS}),
      axios.get(COMMENTS_URL, {headers: HEADERS}),
    ]).then(values => {
      this.setState({
        posts: values[0].data,
        users: values[1].data,
        comments: values[2].data,
        isLoading: false,
      }, () => {
        let usersPosts = [];
        this.state.posts.forEach((post) => {
          const filteredUsersList = this.state.users.filter((user) => user.id === post.userId);
          filteredUsersList.forEach((user) => {
            const merged = {...user, ...post};
            usersPosts.push(merged);
          });
        });

        this.setState({
          usersPosts
        }, () => {
          this.setState({
            filtered: this.state.usersPosts
          });
        });
      });
    }).catch((error) => {
      console.log(`Data fetch error: ${error}`);
    });
  };

  filterList(e) {
    let updatedList = this.state.usersPosts;
    
    updatedList = updatedList.filter((item) => {
      return item.name.toLowerCase().search(
        e.target.value.toLowerCase()) !== -1;
      });

    this.setState({
      filtered: updatedList
    });
  };

  render() {
    const { filtered, comments, isLoading } = this.state;
    const { history } = this.props;

    return (
      <Wrapper>
        <Helmet>
          <title>M&M App</title>
          <meta name="description" content="M&M App" />
        </Helmet>
        <LogoutButton history={history} />
        <SearchInput type="text" className="input" onChange={this.filterList} placeholder="Search users..." />

        { isLoading ? (
          <LoadingIndicator />
        ) : (
          <PostsList usersPosts={filtered} comments={comments} />
        )}

        { filtered.length === 0 && !isLoading &&
          <div className="container">
            <StyledParagraph className="alert alert-danger">User not found</StyledParagraph>
          </div>
        }
      </Wrapper>
    );
  }
}

export default WithHelloMessage(HomePage);
