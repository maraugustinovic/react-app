import styled from 'styled-components';

export const Wrapper = styled.div`
  
`;

export const SearchInput = styled.input`
  width: 50%;
  margin: auto;
  margin-bottom: 20px;
`

export const StyledParagraph = styled.p`
  text-align: center;
`
