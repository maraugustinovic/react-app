import store from 'store';
import {
  EMAIL,
  PASSWORD,
  IS_LOGGED_IN,
} from '../../const'

class Auth {
  login(val, cb) {
    if ((val.email === EMAIL && val.password === PASSWORD)) {
      store.set(IS_LOGGED_IN, true);
      cb();
    }
  }

  logout(cb) {
    store.set(IS_LOGGED_IN, false);
    cb();
  }

  isAuthenticated() {
    return store.get(IS_LOGGED_IN);
  }
}

export default new Auth();
