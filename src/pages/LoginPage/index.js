import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { Helmet } from 'react-helmet';
import WithHelloMessage from '../../components/HelloComponent';
import Auth from '../LoginPage/Auth';
import { EMAIL, PASSWORD } from '../../const'

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isWrongCredentials: false,
    }

    this.onSubmit = this.onSubmit.bind(this);
    this.checkCredentials = this.checkCredentials.bind(this);
  }

  componentDidMount() {
    const { history } = this.props;
  
    if (Auth.isAuthenticated()) {
      history.push('/app');
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timerHandle); 
  }

  checkCredentials(){
    const { email, password } = this.props;

    if (email !== EMAIL || password !== PASSWORD) {
      this.timerHandle = setTimeout(() => {
        this.setState({
          isWrongCredentials: true,
        })
      }, 500)
    }
  }

  onSubmit(values, actions) {
    const { setSubmitting } = actions;
    setSubmitting(false);
    this.checkCredentials();

    const { history } = this.props;
  
    Auth.login(values, () => {
      history.push('/app');
    });
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>M&M | Login</title>
        </Helmet>
        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={this.onSubmit}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email()
              .required("Required"),
            password: Yup.string()
              .required("No password provided.")
              .min(8, "Password is too short - should be 8 chars minimum.")
              .matches(/(?=.*[0-9])/, "Password must contain a number.")
          })}
        >
          {props => {
            const {
              values,
              touched,
              errors,
              handleChange,
              handleBlur,
              handleSubmit
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label htmlFor="email">Email</label>
                <input
                  name="email"
                  type="text"
                  placeholder="Enter your email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.email && touched.email && "error"}
                />
                {errors.email && touched.email && (
                  <div className="input-feedback">{errors.email}</div>
                )}
                <label htmlFor="password">Password</label>
                <input
                  name="password"
                  type="password"
                  placeholder="Enter your password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.password && touched.password && "error"}
                />
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}
                {this.state.isWrongCredentials && (
                  <div className="input-feedback">Wrong email or password</div>
                )}
                <button type="submit">
                  Login
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    );
  }
}

export default WithHelloMessage(LoginPage);
