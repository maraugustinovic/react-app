import React from "react";
import { Route, Redirect } from "react-router-dom";
import Auth from "./LoginPage/Auth";
import WithHelloMessage from '../components/HelloComponent';

const ProtectedRoute = ({
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (Auth.isAuthenticated()) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};

export default WithHelloMessage(ProtectedRoute);
