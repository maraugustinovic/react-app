import React from 'react';
import axios from 'axios';
import { Helmet } from 'react-helmet';
import { Wrapper } from './Wrapper';
import WithHelloMessage from '../../components/HelloComponent';
import LoadingIndicator from '../../components/LoadingIndicator'
import Post from '../../components/Post'
import LogoutButton from '../../components/LogoutButton'
import {
  getSpecificPost,
  getSpecificPostComments,
  USERS_URL,
  HEADERS,
 } from '../../const';

class PostPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      post: '',
      users: [],
      comments: [],
      usersPost: [],
      isLoading: true,
    };

    this.getPostData = this.getPostData.bind(this);
  }

  componentDidMount() {
    const { id: postId } = this.props.match.params;
    this.getPostData(postId);
  }

  getPostData(postId) {
    Promise.all([
      axios.get(getSpecificPost(postId), {headers: HEADERS}),
      axios.get(USERS_URL, {headers: HEADERS}),
      axios.get(getSpecificPostComments(postId), {headers: HEADERS}),
    ]).then(values => {
      this.setState({
        post: values[0].data,
        users: values[1].data,
        comments: values[2].data,
        isLoading: false,
      }, () => {
        let usersPost = [];
        const filteredUsersList = this.state.users.filter((user) => user.id === this.state.post.userId);
        filteredUsersList.forEach((user) => {
          usersPost = {...user, ...this.state.post};
        });

        this.setState({
          usersPost
        });
      });
    }).catch((error) => {
      console.log(`Data fetch error: ${error}`);
    });
  };

  render() {
    const { usersPost, comments, isLoading } = this.state;
    const { history } = this.props;

    return (
      <Wrapper>
        <Helmet>
          <title>M&M Post</title>
          <meta name="description" content="M&M Post" />
        </Helmet>
        <LogoutButton history={history} />

        { isLoading ? (
          <LoadingIndicator />
        ) : (
          <Post usersPost={usersPost} comments={comments} />
        )}
      </Wrapper>
    );
  }
}

export default WithHelloMessage(PostPage);
