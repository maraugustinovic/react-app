# react-showcase-app

## Installation:
  Run `yarn`

## Running the app:
  Run `yarn start`

## Production build and run:
  Run:
  ```
  yarn build
  yarn global add serve
  serve -s build
  ```